<?php
include_once 'system/core/absDB.class.php';
include_once 'system/classes/db.class.php';
include_once 'system/config/config.class.php';

$db = db::getInstance();
$config = config::getInstance();
$sql = [];

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'sef`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'sef` (
            `id`            INT(11)                     NOT NULL        AUTO_INCREMENT,
            `link`          VARCHAR(255) UNIQUE         NOT NULL,
            `alias`         VARCHAR(255) UNIQUE         NOT NULL,
            
            PRIMARY KEY (`id`)
        )';


$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'admin_settings`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'admin_settings` (
            `id`  		   INT(11) 			NOT NULL    AUTO_INCREMENT,
            `data`         VARCHAR(100) 	NOT NULL,
            `type`         ENUM("seller_reg_active","buyer_cart_active") 	NOT NULL,
            
            PRIMARY KEY (`id`),
            KEY `type` (`type`)
        )';


$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'sellers`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'sellers` (
            `id`  		INT(11) 		                NOT NULL    AUTO_INCREMENT,
            `password`	VARCHAR(255)        	        NOT NULL,
            `phone`		VARCHAR(20)		UNIQUE          NOT NULL,
            `fio`       VARCHAR(100) 	                NOT NULL,
            `email`     VARCHAR(100) 	        UNIQUE  NOT NULL,	
            `balance`   DECIMAL(12,2)                   NOT NULL    DEFAULT 0,
            `active`    TINYINT(1)      UNSIGNED        NOT NULL    DEFAULT 1,
            `date` 		INT(11)         UNSIGNED        NOT NULL,
            
            PRIMARY KEY (`id`),
            KEY `password` (`password`),
            KEY `balance` (`balance`),
            KEY `active` (`active`)
        )';


$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'buyers`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'buyers` (
            `id`  		INT(11) 		         NOT NULL    AUTO_INCREMENT,
            `phone`		VARCHAR(20)		UNIQUE   NOT NULL,
            `fio`       VARCHAR(100) 	         NOT NULL,
            `email`     VARCHAR(100) 	UNIQUE   NOT NULL    DEFAULT "",	
            `summ`      DECIMAL(12,2)            NOT NULL    DEFAULT 0,
            `active`    TINYINT(1)      UNSIGNED NOT NULL    DEFAULT 1,
            `count_ord` INT(11)                  NOT NULL    DEFAULT 0,
            `date` 		INT(11)         UNSIGNED NOT NULL,
                    
            PRIMARY KEY (`id`),
            KEY `active` (`active`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'shops`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'shops` (
            `id`  		INT(11) 		            NOT NULL AUTO_INCREMENT,
            `seller_id` INT(11) 	UNSIGNED	    NOT NULL,
            `password`	VARCHAR(255)	            NOT NULL,
            `phone`		VARCHAR(20)		            NOT NULL,
            `name`      VARCHAR(32) 	            NOT NULL,
            `map_label` VARCHAR(255)	            NOT	NULL	DEFAULT "",
            `country`   VARCHAR(255)	            NOT	NULL	DEFAULT "",
            `city`		VARCHAR(255)	            NOT	NULL	DEFAULT "",
            `street_home` VARCHAR(255)	            NOT	NULL	DEFAULT "",
            `type`      ENUM("internet","uni")      NOT NULL    DEFAULT "internet",
            `price_on`  TINYINT(1)		            NOT NULL	DEFAULT 1, /* warning: when it`s 0 it does not mean that shop has not set prices */	
            `rating`    INT(3)                      NOT NULL    DEFAULT 75,
            `date`      INT(11)     UNSIGNED        NOT NULL,
            `active`    TINYINT(1)	UNSIGNED        NOT NULL	DEFAULT 0, 
                    /*if the shop is active, it means:
                    1 - at least the shop has one row of the prices table where 
                            both price and quantity are more than 0 
                    2 - the shop`s price_on field is 1
                    3 - the shop has opened all of new orders (except shop type) and able to get new ones
                    4 - the shop`s owner`s balance is available to get new orders
                    5 - the shop supports at least one of the types of deliveries or the shop`s type is uni
                    6 - the shop must work at least one day for a week
                    7 - the shop`s owner is active*/	
            
            `ex_radius`    INT(11)  	 UNSIGNED           NOT NULL    DEFAULT 0,
            `ex_min_summ`  DECIMAL(12,2)                    NOT NULL    DEFAULT 0,
            `ex_price`     DECIMAL(12,2)                    NOT NULL    DEFAULT 0,
            `ex_free`      DECIMAL(12,2)                    NOT NULL    DEFAULT 0,
                        
            `td_get_order_until` INT(11) UNSIGNED           NOT NULL    DEFAULT 0,
            `td_min_summ`  DECIMAL(12,2)                    NOT NULL    DEFAULT 0,
            `td_price`     DECIMAL(12,2)                    NOT NULL    DEFAULT 0,
            `td_free`      DECIMAL(12,2)                    NOT NULL    DEFAULT 0,
                        
            `tm_get_order_until` INT(11) UNSIGNED           NOT NULL    DEFAULT 0,
            `tm_min_summ`  DECIMAL(12,2)                    NOT NULL    DEFAULT 0,
            `tm_price`     DECIMAL(12,2)                    NOT NULL    DEFAULT 0,
            `tm_free`      DECIMAL(12,2)                    NOT NULL    DEFAULT 0,
                        
            `ukr_pay`       ENUM("after","")                  NOT NULL DEFAULT "",
            `ukr_min_summ`  DECIMAL(12,2)                     NOT NULL    DEFAULT 0,
            `ukr_price`     DECIMAL(12,2)                     NOT NULL    DEFAULT 0,
            `ukr_free`      DECIMAL(12,2)                     NOT NULL    DEFAULT 0,
                        
            `d1_start`      INT(11)  UNSIGNED                  NOT NULL    DEFAULT 0,
            `d2_start`      INT(11)  UNSIGNED                  NOT NULL    DEFAULT 0,
            `d3_start`      INT(11)  UNSIGNED                  NOT NULL    DEFAULT 0,
            `d4_start`      INT(11)  UNSIGNED                  NOT NULL    DEFAULT 0,
            `d5_start`      INT(11)  UNSIGNED                  NOT NULL    DEFAULT 0,
            `d6_start`      INT(11)  UNSIGNED                  NOT NULL    DEFAULT 0,
            `d7_start`      INT(11)  UNSIGNED                  NOT NULL    DEFAULT 0,
                        
            `d1_end`      INT(11)    UNSIGNED                NOT NULL    DEFAULT 0,
            `d2_end`      INT(11)    UNSIGNED                NOT NULL    DEFAULT 0,
            `d3_end`      INT(11)    UNSIGNED                NOT NULL    DEFAULT 0,
            `d4_end`      INT(11)    UNSIGNED                NOT NULL    DEFAULT 0,
            `d5_end`      INT(11)    UNSIGNED                NOT NULL    DEFAULT 0,
            `d6_end`      INT(11)    UNSIGNED                NOT NULL    DEFAULT 0,
            `d7_end`      INT(11)    UNSIGNED                NOT NULL    DEFAULT 0,
                                
            PRIMARY KEY (`id`),
            KEY `seller_id` (`seller_id`),
            KEY `city` (`city`),
            KEY `price_on` (`price_on`),
            KEY `active` (`active`),
            
            KEY `ex_radius` (`ex_radius`),
            KEY `ex_min_summ` (`ex_min_summ`),
            KEY `td_get_order_until` (`td_get_order_until`),
            KEY `td_min_summ` (`td_min_summ`),
            KEY `tm_get_order_until` (`tm_get_order_until`),
            KEY `tm_min_summ` (`tm_min_summ`),
            KEY `ukr_pay` (`ukr_pay`),
            KEY `ukr_min_summ` (`ukr_min_summ`),
            
            KEY `d1_start` (`d1_start`),
            KEY `d2_start` (`d2_start`),
            KEY `d3_start` (`d3_start`),
            KEY `d4_start` (`d4_start`),
            KEY `d5_start` (`d5_start`),
            KEY `d6_start` (`d6_start`),
            KEY `d7_start` (`d7_start`),
   
            KEY `d1_end`   (`d1_end`  ),
            KEY `d2_end`   (`d2_end`  ),
            KEY `d3_end`   (`d3_end`  ),
            KEY `d4_end`   (`d4_end`  ),
            KEY `d5_end`   (`d5_end`  ),
            KEY `d6_end`   (`d6_end`  ),
            KEY `d7_end`   (`d7_end`  )
            
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'products`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'products` (
            `id`  	             INT(11) 		        NOT NULL    AUTO_INCREMENT,
            `name`	        	 VARCHAR(255)	        NOT NULL,
            `des`                TEXT		            NOT NULL,
            `group_id`           INT(11) UNSIGNED       NOT NULL,
            `brand_id`           INT(11)  UNSIGNED      NOT NULL,
            `photo`              VARCHAR(20)            NOT NULL,
            `level`              DECIMAL(12,5) 	        NOT NULL    DEFAULT 0,
                    
            PRIMARY KEY (`id`),
            KEY `brand_id` (`brand_id`),
            KEY `level` (`level`)
        )';


$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'pd_subgrs`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'pd_subgrs` (
            `id`  		INT(11) 		               NOT NULL    AUTO_INCREMENT,
            `pd_id`     INT(11)    UNSIGNED            NOT NULL,
            `subgr_id`  INT(11)    UNSIGNED            NOT NULL,
                    
            PRIMARY KEY (`id`),
            KEY `pd_id` (`pd_id`),
            KEY `subgr_id` (`subgr_id`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'pd_select_subgrs`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'pd_select_subgrs` (
            `id`  		INT(11) 		               NOT NULL    AUTO_INCREMENT,
            `pd_id`     INT(11)    UNSIGNED            NOT NULL,
            `subgr_id`  INT(11)    UNSIGNED            NOT NULL,
            `pd_code`   BIGINT(16) UNSIGNED UNIQUE     NOT NULL    DEFAULT 0,
                    
            PRIMARY KEY (`id`),
            KEY `pd_id` (`pd_id`),
            KEY `subgr_id` (`subgr_id`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'pd_subcats`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'pd_subcats` (
            `id`  		INT(11) 		        NOT NULL    AUTO_INCREMENT,
            `pd_id`     INT(11) UNSIGNED        NOT NULL,
            `subcat_id` INT(11) UNSIGNED        NOT NULL,
                   
            PRIMARY KEY (`id`),
            KEY `pd_id` (`pd_id`),
            KEY `subcat_id` (`subcat_id`)
        )';


$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'subcat_subgrs`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'subcat_subgrs` (
            `id`  		INT(11) 		        NOT NULL    AUTO_INCREMENT,
            `subcat_id` INT(11)  UNSIGNED       NOT NULL,
            `subgr_id`  INT(11)  UNSIGNED       NOT NULL,
                    
            PRIMARY KEY (`id`),
            KEY `subcat_id` (`subcat_id`),
            KEY `subgr_id` (`subgr_id`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'categories`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'categories` (
            `id`  		 INT(11) 		            NOT NULL    AUTO_INCREMENT,
            `name`    	 VARCHAR(255)	            NOT NULL,
            `parent_id`  INT(11)        UNSIGNED    NOT NULL    DEFAULT 0,
            `level`      DECIMAL(12,5) 	            NOT NULL    DEFAULT 0,
                    
            PRIMARY KEY (`id`),
            KEY `parent_id` (`parent_id`),
            KEY `level` (`level`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'groups`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'groups` (
            `id`  		        INT(11) 		                NOT NULL    AUTO_INCREMENT,
            `name`    	        VARCHAR(255)	                NOT NULL,
            `parent_id`         INT(11)        UNSIGNED		    NOT NULL    DEFAULT 0,
            `level`             DECIMAL(12,5) 	                NOT NULL    DEFAULT 0,
            `parent_for_select` TINYINT(1)     UNSIGNED	        NOT NULL    DEFAULT 0,
                    
            PRIMARY KEY (`id`),
            KEY `parent_id` (`parent_id`),
            KEY `level` (`level`),
            KEY `parent_for_select` (`parent_for_select`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'brands`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'brands` (
            `id`  		 INT(11) 		NOT NULL    AUTO_INCREMENT,
            `name`    	 VARCHAR(255)	NOT NULL,
            `level`      DECIMAL(12,5) 	NOT NULL    DEFAULT 0,
                    
            PRIMARY KEY (`id`),
            KEY `level` (`level`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'brand_subcats`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'brand_subcats` (
            `id`  		INT(11) 		        NOT NULL    AUTO_INCREMENT,
            `brand_id`  INT(11)  UNSIGNED       NOT NULL,
            `subcat_id` INT(11)  UNSIGNED       NOT NULL,
                    
            PRIMARY KEY (`id`),
            KEY `brand_id` (`brand_id`),
            KEY `subcat_id` (`subcat_id`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'prices`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'prices` (
            `id`  		 INT(11) 		                NOT NULL    AUTO_INCREMENT,
            `shop_id`    INT(11) 		UNSIGNED        NOT NULL,
            `pd_code`    BIGINT(16)     UNSIGNED        NOT NULL,
            `price`  	 DECIMAL(12,2) 	                NOT NULL,
            `quantity`   INT(11)        UNSIGNED        NOT NULL    DEFAULT 0,
     
            PRIMARY KEY (`id`),
            KEY `shop_id` (`shop_id`),
            KEY `pd_code` (`pd_code`),
            KEY `price` (`price`),
            KEY `quantity` (`quantity`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'files`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'files` (
            `id`  		 INT(11) 		        NOT NULL    AUTO_INCREMENT,
            `name`  	 VARCHAR(255) UNIQUE    NOT NULL,
            `date` 		 INT(11)      UNSIGNED  NOT NULL,
            `shop_id`    INT(11)      UNSIGNED  NOT NULL    DEFAULT 0, /*IF A FILE BELONGS TO THE ADMIN*/
                    
            PRIMARY KEY (`id`),
            KEY `date` (`date`),
            KEY `shop_id` (`shop_id`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'carts`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'carts` (
            `id`  		   INT(11) 		                    NOT NULL    AUTO_INCREMENT,
            `cart_code`    BIGINT(16)   UNSIGNED            NOT NULL,
            `pd_code`      BIGINT(16)   UNSIGNED            NOT NULL,
            `quantity`     INT(11) 	    UNSIGNED	        NOT NULL       DEFAULT 1,   
            `date`         INT(11) 	    UNSIGNED	        NOT NULL, 

            PRIMARY KEY (`id`),
            KEY `cart_code` (`cart_code`),
            KEY `pd_code` (`pd_code`),
            KEY `quantity` (`quantity`),
            KEY `date` (`date`)
            
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'orders`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'orders` (
            `id`  		   INT(11) 		              NOT NULL      AUTO_INCREMENT,
            `shop_id`      INT(11) 		   UNSIGNED   NOT NULL, 
            `buyer_id`     INT(11) 		   UNSIGNED   NOT NULL, 
            `cart_code`    BIGINT(16)      UNSIGNED   NOT NULL, 
            `country`      VARCHAR(255)               NOT NULL      DEFAULT "",
            `city`   	   VARCHAR(255)               NOT NULL      DEFAULT "",
            `street_home`  VARCHAR(255)               NOT NULL      DEFAULT "",
            `delType`      ENUM("ex", "td", "tm", "ukr", "shop") NOT NULL,
            `delPrice`     DECIMAL(12,2)              NOT NULL,
            `summ`         DECIMAL(12,2)              NOT NULL,     /* WITHOUT DISCOUNT */
            `discount`     TINYINT(3)      UNSIGNED   NOT NULL,
            `date`         INT(11) 		   UNSIGNED   NOT NULL, 
            `seen`         TINYINT(1) 	    UNSIGNED  NOT NULL	    DEFAULT 0, 
            `confirmed`    TINYINT(1) 	    UNSIGNED  NOT NULL	    DEFAULT 0, 
            `seller_paid`  DECIMAL(12,2)              NOT NULL      DEFAULT 0,
            `code_for_com` VARCHAR(15)      UNIQUE    NOT NULL, 
            `ip`    	   BIGINT(20)      UNSIGNED   NOT NULL,

            PRIMARY KEY (`id`),
            KEY `shop_id` (`shop_id`),
            KEY `buyer_id` (`buyer_id`),
            KEY `cart_code` (`cart_code`),
            KEY `delType` (`delType`),
            KEY `date` (`date`),
            KEY `seen` (`seen`),
            KEY `confirmed` (`confirmed`),
            KEY `ip` (`ip`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'order_pds`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'order_pds` (
            `id`  		   INT(11) 		                    NOT NULL    AUTO_INCREMENT,
            `order_id`     INT(11) 	  UNSIGNED              NOT NULL,
            `pd_code`      BIGINT(16) UNSIGNED              NOT NULL,
            `quantity`     INT(11) 	  UNSIGNED              NOT NULL,   
            `price`        DECIMAL(12,2)                    NOT NULL,

            PRIMARY KEY (`id`),
            KEY `order_id` (`order_id`),
            KEY `pd_code` (`pd_code`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'stop_ip`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'stop_ip` (
            `id`  		        INT(11) 		            NOT NULL    AUTO_INCREMENT,
            `ip`     	        BIGINT(20) 	    UNIQUE	    NOT NULL,
            `written_date`      INT(11) 		UNSIGNED	NOT NULL,
            `last_ok_sms_time`  INT(11) 		UNSIGNED	NOT NULL    DEFAULT 0,
            `last_captcha_time` INT(11) 		UNSIGNED	NOT NULL    DEFAULT 0,

            PRIMARY KEY (`id`),
            KEY `last_ok_sms_time` (`last_ok_sms_time`),
            KEY `last_captcha_time` (`last_captcha_time`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'admin_emails`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'admin_emails` ( 
            `id`  		   INT(11) 		            NOT NULL    AUTO_INCREMENT,
            `email`        VARCHAR(100) 	        NOT NULL,
            `type`         ENUM("new_seller","new_code","new_order") 	NOT NULL,

            PRIMARY KEY (`id`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'balance_histories`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'balance_histories` ( 
            `id`  		   INT(11) 		            NOT NULL    AUTO_INCREMENT,
            `seller_id`    INT(11) 		UNSIGNED	NOT NULL,
            `summ`         DECIMAL(12,2) 	        NOT NULL,
            `balance_rest` DECIMAL(12,2) 	        NOT NULL,
            `type`         ENUM("+","-") 	        NOT NULL,
            `date`  	   INT(11) 		UNSIGNED	NOT NULL,
            `comment`      varchar(255)             NOT NULL,

            PRIMARY KEY (`id`),
            KEY `seller_id` (`seller_id`),
            KEY `type` (`type`),
            KEY `date` (`date`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'discounts`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'discounts` ( 
            `id`  		   INT(11) 		            NOT NULL    AUTO_INCREMENT,
            `shop_id`  	   INT(11) 		 UNSIGNED	NOT NULL,
            `discount`     TINYINT(3) 	 UNSIGNED   NOT NULL,
            `summ`         DECIMAL(12,2) 	        NOT NULL,

            PRIMARY KEY (`id`),
            KEY `shop_id` (`shop_id`),
            KEY `discount` (`discount`),
            KEY `summ` (`summ`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'comments`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'comments` ( 
            `id`  		INT(11) 		                    NOT NULL    AUTO_INCREMENT,
            `order_id`  INT(11)     UNSIGNED	 UNIQUE	    NOT NULL,
            `name`	    VARCHAR(255)	                    NOT NULL    DEFAULT "",
            `comment`   text    		                    NOT NULL,
            `date`      INT(11) 	 UNSIGNED               NOT NULL,
            `ball`      TINYINT(3) 	 UNSIGNED               NOT NULL,

            PRIMARY KEY (`id`)
        )';
        
$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'promo_codes`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'promo_codes` ( 
            `id`  		   INT(11) 		                        NOT NULL    AUTO_INCREMENT,
            `generated_date`  INT(11)    UNSIGNED	            NOT NULL,
            `used_date`    INT(11) 		 UNSIGNED	            NOT NULL    DEFAULT 0,
            `code`     	   VARCHAR(100)  UNIQUE	                NOT NULL,
            `summ`         DECIMAL(12,2) 	                    NOT NULL,
            `seller_id`    INT(11) 		 UNSIGNED	            NOT NULL	DEFAULT 0,

            PRIMARY KEY (`id`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'auth_tokens`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'auth_tokens` ( 
            `id`  		   INT(11) 		                        NOT NULL    AUTO_INCREMENT,
            `seller_id`    INT(11) 		 UNSIGNED	            NOT NULL    DEFAULT 0,
            `shop_id`  	   INT(11) 		 UNSIGNED	            NOT NULL    DEFAULT 0,
            `serial_code`  VARCHAR(50)   UNIQUE	                NOT NULL,
            `token`        VARCHAR(50)   UNIQUE	                NOT NULL,
            `ip`           BIGINT(20)    UNSIGNED               NOT NULL    DEFAULT 0,
            `user_agent`   VARCHAR(255)   	                    NOT NULL,
            `del_time`     INT(11) 		 UNSIGNED	            NOT NULL,

            PRIMARY KEY (`id`),
            KEY `seller_id` (`seller_id`),
            KEY `shop_id` (`shop_id`),
            KEY `ip` (`ip`),
            KEY `user_agent` (`user_agent`),
            KEY `del_time` (`del_time`)
        )';

$sql[] = 'DROP TABLE IF EXISTS `'.$config->db_prefix.'ukr_cities`';
$sql[] = 'CREATE TABLE `'.$config->db_prefix.'ukr_cities` ( 
            `id`  		       INT(11) 		                    NOT NULL    AUTO_INCREMENT,
            `rus_name`     	   VARCHAR(70)   UNIQUE	            NOT NULL,
            `additional_names` VARCHAR(255)  UNIQUE	            NOT NULL,

            PRIMARY KEY (`id`)
        )';

$tables =0;
$sql_plus = ' ENGINE = MyISAM
            DEFAULT CHARSET=utf8
            COLLATE=utf8_general_ci
                 ';

foreach ($sql as $value) {
    if (mb_strlen($value)>70) $value.= $sql_plus;
    if ($db->otherQuery($value)) $tables++;
    else exit($db->error);
}

echo 'Created tables '.($tables/2).' OF '.(count($sql)/2);
echo '<br>';
$sql = [];
include 'installData.php';