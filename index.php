<?php
/**
 * Биссимилла!
 * Created by PhpStorm.
 * User: Ads Bat
 * Date: june 24, 2019
 */

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

date_default_timezone_set("Europe/Kiev");
//date_default_timezone_set("Asia/Bishkek");
//date_default_timezone_set('America/Los_Angeles');

require_once 'system/config/config.class.php';
require_once 'system/core/main/runner.class.php';

$runner = new runner();
$runner->run();