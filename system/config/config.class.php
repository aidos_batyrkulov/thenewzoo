<?php
// singleton
class config {
    private static $_instance;
    private $data;

    private function __construct() {
        $domain = 'thenewzoo.local';  // use it in this func's body
        $doc_root = $_SERVER['DOCUMENT_ROOT'].'/'; // use it in this func's body
        $this->data = [
            'doc_root'=>$doc_root,
            'dir_close_files' =>$doc_root.'/close/files/',
            'dir_tpl' =>$doc_root.'system/mvc/view/tpls/',
            'dir_lang' =>$doc_root.'system/mvc/view/lang/',
            'dir_controllers' =>$doc_root.'system/mvc/controllers/',
            'dir_css_for_frontend' =>'//'.$domain.'/open/css/',
            'dir_js_for_frontend' =>'//'.$domain.'/open/js/',
            'dir_brand_for_frontend' =>'//'.$domain.'/open/images/sysimg/brand/',

            'abs_address'=>'http://'.$domain,
            'domain'=>$domain,
            'sef_suffix'=>'.html',

            'example_of_correct_pass'=>'This_is_correct_123',

            'date_format' =>'%d.%m.%Y %H:%M:%S',

            'db_host' =>'localhost',

            'db_user' =>'root',
            'db_password' =>'220297',
/*
            'db_user' =>'mysql',
            'db_password' =>'mysql',*/

            'db_name' =>'zzz',
            'db_prefix' =>'zzz_',
            'db_sq' =>'?',

            'seller_balance_minus_limit'=>20
        ];
    }
    private function __clone () {}

    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __get($name) {
        return $this->data[$name];
    }
}
