<?php
class myWebSite extends absWebSite {
    private $session;

    private $head;
    //private $hornav;

    private $top;
    //private $left;

    private $footer;

    public function __construct() {
        parent::__construct(config::getInstance());
        $this->session = session::getInstance();
    }

    public function setHead($head) {
        $this->head = $head;
    }
    public function setTop($top) {
        $this->top = $top;
    }
    public function setFooter($footer) {
        $this->footer = $footer;
    }
    public function setMessage($message) {
        $this->message = $message;
        $this->session->message = $message;
    }
    public function isMessage(){return (!empty($this->session->message));}

    public function getHead() {return $this->head;}
    public function getTop() {return $this->top;}
    public function getFooter() {return $this->footer;}
    public function getMessage() {
        $message = (!empty($this->session->message) ? $this->session->message : false );
        $this->session->message = null;
        $this->message = null;
        return $message;
    }


    public function display() {
        return $this->html;
    }

}