<?php

abstract class absMyModel extends absModel {
    protected $config;
	
	public function __construct($table) {
	    $this->config = config::getInstance();
		parent::__construct($table, $this->config->date_format);
	}

	
}