<?php
// singlton
class myAuth extends absAuth {
    protected static $_instance;

    protected $cartCode = null;

    protected  function __construct() {
        parent::__construct();
        if (!empty($this->request->cookie['cart_code']))
            $this->setCartCode($this->request->cookie['cart_code']);
    }

    protected function getRoleNameForUnAuthed() {
        return 'buyer';
    }

    public function authorization($login, $password) {

    }

    public static  function getInstance() {
        if (is_null(self::$_instance)) {
        self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function setCartCode($cartCode) {
        // here you can check $cartCode
        $this->cartCode = $cartCode;
    }

    public function getCartCode() {
        return $this->cartCode;
    }
}