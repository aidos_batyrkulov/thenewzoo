<?php
abstract class absMyController extends absController  {
    protected $lang;
    protected $config;
    protected $session;
    protected $request;
    protected $myAuth;

    public function __construct(myWebSite $myWebSite) {
        parent::__construct($myWebSite);
        $this->lang = lang::getInstance();
        $this->config = config::getInstance();
        $this->session = session::getInstance();
        $this->request = request::getInstance();
        $this->myAuth = myAuth::getInstance();
    }

    protected final function generateOthers() {
        $this->setHead();
        $this->setTop();
        $this->setFooter();
        $this->setHtml();
    }

    private function setHead() {
        $head = new head();
        $this->lang->load('head');

        if ($this->myAuth->getRole() != 'buyer') {
            $this->myWebSite->addStyle($this->myAuth->getRole());
        } else {
            $this->myWebSite->addStyle('loading');
            $this->myWebSite->addStyle('style');
        }
        $this->myWebSite->addJs('jquery.min');
        $this->myWebSite->addJs('jquery.cookie');
        $this->myWebSite->addJs('jquery.maskedinput.min');

        $this->myWebSite->addLink(['href'=>"favicon.png", 'rel'=>"shortcut icon", 'type'=>"image/x-icon"]);

        $this->myWebSite->addMetaTag(['name'=>'description', 'content'=>$this->lang->meta_des]);
        $this->myWebSite->addMetaTag(['name'=>'viewport', 'content'=>'width=device-width, initial-scale=1.0, user-scalable=1, minimum-scale=1.0, maximum-scale=1.0']);
        $this->myWebSite->addMetaTag(['http-equiv'=>'Content-Type', 'content'=>'text/html; charset=utf-8']);
        $this->myWebSite->addMetaTag(['property'=>'og:title', 'content'=>$this->lang->og_title]);
        $this->myWebSite->addMetaTag(['property'=>'og:description', 'content'=>$this->lang->og_des]);
        $this->myWebSite->addMetaTag(['property'=>'og:image', 'content'=>$this->config->abs_address.'/og.png']);
        $this->myWebSite->addMetaTag(['property'=>'og:image:width', 'content'=>'968']);
        $this->myWebSite->addMetaTag(['property'=>'og:image:height', 'content'=>'504']);
        $this->myWebSite->addMetaTag(['property'=>'og:url', 'content'=>$this->config->abs_address]);
        $this->myWebSite->addMetaTag(['property'=>'og:type', 'content'=>'website']);
        $this->myWebSite->addMetaTag(['property'=>'og:site_name', 'content'=>'ZooSkop.com']);

        $head->title = $this->myWebSite->getTitle();
        $head->styles = $this->myWebSite->getStyles();
        $head->jses = $this->myWebSite->getJses();
        $head->metaTags = $this->myWebSite->getMetaTags();
        $head->links = $this->myWebSite->getLinks();

        $this->myWebSite->setHead($head);
    }

    /*private function setHornav($hornav) {
        $this->hornav = $hornav;
    }*/
    private function setTop() {
        $top = new top();
        $this->lang->load('top');

        $lineMenuItems = [];
        $blockMenuItems = [];
        if ($this->myAuth->getRole()=='admin') {
            $lineMenuItems = [
                ['name'=>'navUsers', 'active'=>($this->myWebSite->getCurrentPageName()=='admUsers'), 'link'=>url::get('admUsers'), 'counter'=>false],
                ['name'=>'navOrders', 'active'=>($this->myWebSite->getCurrentPageName()=='admOrders'), 'link'=>url::get('admOrders'), 'counter'=>false],
                ['name'=>'navBuyers', 'active'=>($this->myWebSite->getCurrentPageName()=='admBuyers'), 'link'=>url::get('admBuyers'), 'counter'=>false]
            ];
            $blockMenuItems = [
                ['name'=>'promo', 'link'=>url::get('admPromo'), 'social'=>false],
                ['name'=>'payments', 'link'=>url::get('admTrans'), 'social'=>false],
                ['name'=>'settings', 'link'=>url::get('admSettings'), 'social'=>false]
            ];
        } else if ($this->myAuth->getRole()=='user') {
            $lineMenuItems = [
                ['name'=>'navOrders', 'active'=>($this->myWebSite->getCurrentPageName()=='userOrders'), 'link'=>url::get('userOrders'), 'counter'=>false],
                ['name'=>'navShops', 'active'=>($this->myWebSite->getCurrentPageName()=='shops'), 'link'=>url::get('shops'), 'counter'=>false],
                ['name'=>'navFeedbacks', 'active'=>($this->myWebSite->getCurrentPageName()=='feedbacks'), 'link'=>url::get('feedbacks'), 'counter'=>false]
            ];
            $blockMenuItems = [
                ['name'=>'addshop', 'link'=>url::get('addshop'), 'social'=>false],
                ['name'=>'balance', 'link'=>url::get('balance'), 'social'=>false],
                ['name'=>'settings', 'link'=>url::get('settings'), 'social'=>false]
            ];
            $seller = new sellers();
            $seller->load($this->myAuth->getID());
            if ($seller->balance<=$this->config->seller_balance_minus_limit)
                $this->myWebSite->setMessage($this->lang->you_should_top_up_balance);
        } else if ($this->myAuth->getRole()=='shop') {
            $lineMenuItems = [
                ['name'=>'navOrders', 'active'=>($this->myWebSite->getCurrentPageName()=='orders'), 'link'=>url::get('orders'), 'counter'=>orders::getCountOfNewOrdersOnShopID($this->myAuth->getID())],
                ['name'=>'navPrice', 'active'=>($this->myWebSite->getCurrentPageName()=='products'), 'link'=>url::get('products'), 'counter'=>prices::getCountWherePriceOrQuantityIsZeroOnShopID($this->myAuth->getID())],
                ['name'=>'navCatalog', 'active'=>(($this->myWebSite->getCurrentPageName()=='brands') || ($this->myWebSite->getCurrentPageName()=='addgoods')), 'link'=>url::get('brands'), 'counter'=>false]
            ];
            $blockMenuItems = [
                ['name'=>'info', 'link'=>url::get('info'), 'social'=>false]
            ];
            $shop =  new shops();
            $shop->load($this->myAuth->getID());
            $atLeastOne = prices::areThereAtLeastOnePriceWherePriceOrQuantityIsZeroOnShopID($this->myAuth->getID());
            if (($shop->price_on==0) || (!$atLeastOne))
                $this->myWebSite->setMessage($this->lang->price_off_or_end_products);
        } else {
            $lineMenuItems = [
                ['name'=>'navCategory', 'active'=>($this->myWebSite->getCurrentPageName()=='main'), 'link'=>'/', 'counter'=>false],
                ['name'=>'navGoods', 'active'=>($this->myWebSite->getCurrentPageName()=='catalog'), 'link'=>url::get('catalog'), 'counter'=>carts::getCountOfSummOfAllQuantitiesOnCartCode($this->myAuth->getCartCode())],
                ['name'=>'navShops', 'active'=>($this->myWebSite->getCurrentPageName()=='cart'), 'link'=>url::get('cart'), 'counter'=>false]
            ];
            $blockMenuItems = [
                ['name'=>'login', 'link'=>url::get('login'), 'social'=>false],
                ['name'=>'register', 'link'=>url::get('register'), 'social'=>false],
                ['name'=>'about', 'link'=>url::get('about'), 'social'=>false],
                ['name'=>'fb', 'link'=>'http://www.facebook.com/sharer.php?u=https://zooskop.com&src=sp', 'social'=>true],
                ['name'=>'in', 'link'=>'https://www.instagram.com/zooskop/', 'social'=>true],
                ['name'=>'vb', 'link'=>'https://invite.viber.com/?g2=AQBPFFQ26ZPz6Ug2pmliuaNPlF8p2kbODpmTiVbaf9OUdrqCLiRdkz%2FZZq6J1Kji&lang=ru', 'social'=>true]
            ];
        }
        if (($this->myAuth->getRole()=='admin') || ($this->myAuth->getRole()=='user') || ($this->myAuth->getRole()=='shop'))
            $blockMenuItems = [ ['name'=>'logout', 'link'=>url::get('logout'), 'social'=>false] ];

        $lineMenu = ['logo'=>true, 'blockMenuButton'=>true, 'items'=> $lineMenuItems ];
        $blockMenu = [
            'btnCloseName'=>(($this->myAuth->getRole()=='user') ? 'btnClose' : 'close'),
            'itemsBlockName'=>(($this->myAuth->getRole()=='buyer') ? 'item' : 'menu'),
            'items'=>$blockMenuItems
        ];
        if ($this->myWebSite->isMessage())
            $top->alert = $this->myWebSite->getMessage();
        $top->lineMenu = $lineMenu;
        $top->blockMenu = $blockMenu;

        $this->myWebSite->setTop($top);
    }
    /*private function setLeft() {

    }*/

    private function setFooter() {
        $footer = new footer();
        $this->myWebSite->setFooter($footer);
    }

    private function setHtml() {
        $html = new html();

        $html->head = $this->myWebSite->getHead();
        $html->top = $this->myWebSite->getTop();
        //$html->hornav = $this->myWebSite->getHornav();
        //$html->left= $this->myWebSite->getLeft();
        $html->content = $this->myWebSite->getContent();
        $html->footer = $this->myWebSite->getFooter();
        $this->myWebSite->setHtml($html);
    }

}
