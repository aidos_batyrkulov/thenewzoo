<?php
// singleton
class db extends  absDB {
    private static $_instance;

    protected function __construct($db_host, $db_user, $db_password, $db_name, $sq, $prefix)
    {
        parent::__construct($db_host, $db_user, $db_password, $db_name, $sq, $prefix);
    }

    private function __clone() {}

    public static function getInstance() {
        if (is_null(self::$_instance)) {
            $config = config::getInstance();
            self::$_instance = new self($config->db_host, $config->db_user, $config->db_password, $config->db_name, $config->db_sq, $config->db_prefix);
        }
        return self::$_instance;
    }
}