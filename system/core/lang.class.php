<?php
// singleton
class lang {
    private $request;
    private $config;
    private static $_instance;
    private $data =[];
    private $lang = 'ru';

    private function __construct() {
        $this->request = request::getInstance();
        $this->config = config::getInstance();
        $this->setLang();
        $this->load('basic');
    }
    private function __clone () {}

    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __get($name) {
        return $this->data[$name];
    }

    public function getLang() {
        return $this->lang;
    }

    public function setLang($lang=null) {
        if ($lang) {
            setcookie('lang', $lang, time()+9999999, '/');
            $this->lang = $lang;
        } else {
            switch (@$this->request->cookie['lang']) {
                case 'en': $this->lang = 'en'; break;
                default: $this->lang = 'ru';
            }
            setcookie('lang', $this->lang, time()+9999999, '/');
        }
    }


    public function load($langFile) {
        $newArr = include $this->config->dir_lang.$this->lang.'/'.$langFile.'.'.$this->lang.'.php';
        $this->data = array_merge($this->data, $newArr);
    }
}
