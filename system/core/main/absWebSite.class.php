<?php
abstract class absWebSite {
    private $title;
    private $currentPageName;
    protected $message;
    private $metaTags=[];
    private $links = [];
    private $jses=[];
    private $styles=[];

    protected $content;
    protected $html;

    protected $config;

    public function __construct($config) {
        $this->config = $config;
    }


    public function setTitle($title) {
        $this->title = $title;
    }
    public function setCurrentPageName($currentPageName) {
        $this->currentPageName = $currentPageName;
    }
    public function addMetaTag($meta) {
        $this->metaTags[] = $meta;
    }
    public function addLink($link) {
        $this->links[] = $link;
    }
    public function addJs($js) {
        $this->jses[] = $js;
    }
    public function addStyle($style) {
        $this->styles[] = $style;
    }
    public function setContent($content) {
        $this->content = $content;
    }
    public function setHtml($html) {
        $this->html = $html;
    }
    public abstract function setMessage($message);
    public abstract function isMessage();
    public abstract function getMessage();

    public function getTitle() {return $this->title;}
    public function getCurrentPageName() {return $this->currentPageName;}
    public function getJses() {return $this->jses;}
    public function getMetaTags() {return $this->metaTags;}
    public function getLinks() {return $this->links;}
    public function getStyles() {return $this->styles;}

    public function getContent() {return $this->content;}
    public function getHtml() {return $this->html;}
}