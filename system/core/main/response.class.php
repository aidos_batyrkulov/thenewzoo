<?php
// singleton
class response {
    private static $_instance;

    private function __construct() {}
    private function __clone () {}

    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    public function show($data) {
        echo $data;
    }
}