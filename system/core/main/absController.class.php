<?php
abstract class absController {
    protected $myWebSite;

    public function __construct(myWebSite $myWebSite){
        $this->myWebSite = $myWebSite;
    }

    // it's static because it is also for API functions
    public static function isAccess() {
        return true;
    }

    public function generateBlocks() {
        $this->generateContent();
        $this->generateOthers();
    }

    protected abstract function generateContent();

    protected abstract function generateOthers();
/*
    protected function getBaseHornav() {
        $hornav = new hornav();
        $hornav->addData('Main');
        return $hornav;
    }
*/
    /*
     * API methods are in controllers just for place!
     * They have not any connect with controllers!
     * They have thier own system!
     */
    public static function apiStartSettings () {
        header("Content-type: application/json; charset=utf-8");
    }

    protected static function getObjectsForApiFunctions() {
        return ['myAuth'=>myAuth::getInstance(), 'request'=>request::getInstance()];
    }

    public static function apiFunctionNotFound() {

    }
}