<?php
class requestAnalyzer {
    private static $_instance;
    private $result;

    private function __clone () {}

    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    private function __construct() {
            $request =  request::getInstance();
            $uri = $request->server['REQUEST_URI'];
            $urlAndQs = explode("?", $uri);
            $res = explode('/', $urlAndQs[0]);
            if (empty($res[2]))
                $this->result='page';
            else
                $this->result='api';
    }

    public function getResult() {
        return $this->result;
    }
}