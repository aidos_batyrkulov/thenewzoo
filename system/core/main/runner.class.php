<?php
// site starts here
class runner {
    private $config;

    public function __construct() {
        $this->config = config::getInstance();
    }

    public function run() {
        try {
            spl_autoload_register(function ($class_name) {
                $this->classAutoLoad($class_name);
            });
            absModel::setDB(db::getInstance());
            $requestAnalyzer = requestAnalyzer::getInstance();
            $analyzeResult = $requestAnalyzer->getResult();
            $controllerAndApiMethod = url::getControllerAndApiMethod();
            $controllerAndApiMethod['controller'] .= 'Controller';
            if (!$controllerAndApiMethod['controller']::isAccess()) exit('You don`t have access to this page!');
            $response = response::getInstance();
            if ($analyzeResult === 'page') {
                $myWebSite = new myWebSite();
                $controller = new $controllerAndApiMethod['controller']($myWebSite);
                $controller->generateBlocks();
                $response->show($myWebSite->display());
            } else if ($analyzeResult === 'api') {
                absController::apiStartSettings();
                $apiResult = $controllerAndApiMethod['controller']::$controllerAndApiMethod['apiMethod']();
                $response->show($apiResult);
            }
        } catch (Throwable $t) {
            // log
            echo 'FILE: '.$t->getFile();
            echo '<br>';
            echo 'LINE: '.$t->getLine();
            echo '<br>';
            echo 'MESSAGE: '.$t->getMessage();
            //exit
            exit('<br> SYSTEM ERROR');
        }
    }

    private function classAutoLoad($className) {
        $pathes = ['classes/', 'config/', 'core/', 'core/main/', 'core/validators/', 'libs/', 'mvc/controllers/',
                    'mvc/models/', 'mvc/view/modules/'];
        $dir_system = $this->config->doc_root.'system/';
        foreach ($pathes as $path) {
            $file = $dir_system.$path.$className.'.class.php';
            if (file_exists($file)) {
                require_once $file;
                break;
            }
        }
    }
}