<?php
/*
 * The validators must know what they use instead of NULL
 * ( ex: empty string for strings, 0 for int and float )
 * The validators of the fields that have TEXT or
 * DATE (don't confuse with the fields that have name "Date" but have type INT) or ... type use NULL of course.
 * The validators' checking pattern must not include NULL (or analog of NULL if it exists) because
 * there is a "required" var for that!
 */
abstract class absValidator {
	protected $lang;
	protected $data;
	protected $required;
	private $error = false;
	
	public function __construct($data, $required) {
	    $this->lang = lang::getInstance();
		$this->data = $data;
		$this->required = $required;
		$this->validate();
	}
	
	abstract protected function validate();
	
	public function getError() {
		return $this->error;
	}
	
	public function isValid() {
		if ($this->error===false) return true;
		else return false;
	}
	
	protected function setError($code) {
		$this->error = $code;
	}

	
}
