<?php

class validateEmail extends absValidator {

	const MAX_LEN = 100;

	protected function validate() {
		$data = $this->data;
		if (mb_strlen($data) == 0) $this->setError($this->lang->error_email_empty);
		else {
			if (mb_strlen($data) > self::MAX_LEN) $this->setError($this->lang->error_email_max_len);
			else {
				$pattern = "/^[a-z0-9_][a-z0-9\._-]*@([a-z0-9]+([a-z0-9-]*[a-z0-9]+)*\.)+[a-z]+$/i";
				if (!preg_match($pattern, $data)) $this->setError($this->lang->error_email_invalid);
			}
		}
	}
	
}
