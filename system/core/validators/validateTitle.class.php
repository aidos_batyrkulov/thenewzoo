<?php

class validateTitle extends absValidator {
	
	const MAX_LEN = 100;
	
	protected function validate() {
		$data = $this->data;
		if (mb_strlen($data) == 0) $this->setError($this->lang->error_empty_value);
		elseif (mb_strlen($data) > self::MAX_LEN) $this->setError($this->lang->error_max_len);
	}
	
}

?>