<?php

class validateURI extends absValidator {
	
	const MAX_LEN = 255;
	
	protected function validate() {
		$data = $this->data;
		if (mb_strlen($data) > self::MAX_LEN) $this->setError($this->lang->error_unknown);
		else {
			$pattern = "~^(?:/[a-z0-9.,_@%&?+=\~/-]*)?(?:#[^ '\"&<>]*)?$~i";
			if (!preg_match($pattern, $data)) $this->setError($this->lang->error_unknown);
		}
	}
	
}

?>