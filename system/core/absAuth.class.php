<?php
abstract class absAuth {
    protected $session;
    protected $request;
    protected $ID = 0;
    protected $role = null;

    protected function __construct() {
        $this->session = session::getInstance();
        $this->request = request::getInstance();
        if ($this->session->ID) {
            $this->ID = $this->session->ID;
            $this->role = $this->session->role;
        } else
            $this->role= $this->getRoleNameForUnAuthed();
    }

    protected abstract function getRoleNameForUnAuthed ();

    protected function __clone () {}

    public function deAuth() {
        $this->ID = 0;
        $this->role = null;
        $this->session->destroy();
    }


    public function isAuth() {
        return $this->ID;
    }

    public function getID() {
        return $this->ID;
    }

    public function getRole() {
        return $this->role;
    }
}