<?php
// singleton
class session {
    private static $_instance;
    private $data = array();

    private function __construct() {
        if ((isset($_COOKIE[session_name()]))&&(!empty($_COOKIE[session_name()]))&&(session_status()!==PHP_SESSION_ACTIVE)) {
            session_start();
            if (!isset($_SESSION['main'])) {
                $_SESSION['main'] = array();
            }

            $this->data =& $_SESSION['main'];
        }

    }

    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone () {}


    public function start() {
        if (session_status()!==PHP_SESSION_ACTIVE) {
            session_start();
            if (!isset($_SESSION['main'])) {
                $_SESSION['main'] = array();
            }

            $this->data =& $_SESSION['main'];
        }
    }

    public function isStarted() {
        if (session_status()!==PHP_SESSION_ACTIVE)
            return FALSE;
        else
            return TRUE;
    }


    public function __get($name) {
        if ($this->isStarted()) {
            return @$this->data[$name];
        } else {
            if ((isset($_COOKIE[session_name()]))&&(!empty($_COOKIE[session_name()]))) {
                $this->start();
                if (isset($this->data[$name]))
                    return $this->data[$name];
            }
            return null;
        }
    }

    public function __set($name, $value)
    {
        if (!$this->isStarted())
            $this->start();
        $this->data[$name]= $value;
    }

    public function destroy() {
        if ($this->isStarted()) {
            $_SESSION=array();
            session_destroy();
        }
        setcookie(session_name(), '', time() - 2592000, '/');
    }
}

