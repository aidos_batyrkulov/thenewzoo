<?php

class url {

    public static function get($controller, $data = array(), $amp = true, $address = "", $linkToAlias = true) {
        if ($amp) $amp = "&amp;";
        else $amp = "&";
        $uri = '/'.$controller;
        if (count($data) != 0) {
            $uri .= "?";
            foreach ($data as $key => $value) {
                $uri .= "$key=$value".$amp;
            }
            $uri = substr($uri, 0, -strlen($amp));
        }
        if ($linkToAlias) return self::linkToAlias($uri, $address);
        return self::getAbsolute($address, $uri);
    }

    public static function getAbsolute($address, $uri) {
        return $address.$uri;
    }

    public static function current($address = "", $amp = false) {
        $url = self::getAbsolute($address, $_SERVER["REQUEST_URI"]);
        if ($amp) $url = str_replace("&", "&amp;", $url);
        return $url;
    }

    public static function getControllerAndApiMethod() {
        $config = config::getInstance();
        $uri = $_SERVER["REQUEST_URI"];
        $requestAnalyzer = requestAnalyzer::getInstance();
        if ($requestAnalyzer->getResult()=='page')
            $uri = useSef::getRequest($uri);
        if (!$uri) return array('controller' =>'notFound404', 'apiMethod'=>false);
        $urlAndQs = explode("?", $uri);
        if (!empty($urlAndQs[1])) {
            parse_str($urlAndQs[1], $qs_vars);
            $request = request::getInstance();
            $request->addSefDataToGet($qs_vars);
        }
        $controller_name = "main";
        $apiMethod = false;
        if ($uri!=='/') {
            if (($pos = strpos($uri, "?")) !== false)
                $uri = substr($uri, 0, strpos($uri, "?"));
            $routes = explode("/", $uri);
            // The main page can be opened by only one link '/' not '/main'
            if (file_exists($config->dir_controllers.$routes[1].'Controller.class.php') && (  (!empty($routes[2])) || (strcasecmp($routes[1], 'main')!==0)  )) {
                $controller_name = $routes[1];
                if (!empty($routes[2]))
                    $apiMethod = method_exists($controller_name.'Controller', $routes[2]) ? $routes[2] : 'apiFunctionNotFound';
            } else {
                if (empty($routes[2])) $controller_name = 'notFound404';
                else {
                    $controller_name = 'absController';
                    $apiMethod = 'apiFunctionNotFound';
                }
            }
        }

        return array('controller' =>$controller_name, 'apiMethod'=>$apiMethod);
    }


    public static function addGET($url, $name, $value, $amp = true) {
        if (strpos($url, "?") === false) $url = $url."?".$name."=".$value;
        else {
            if ($amp) $amp = "&amp;";
            else $amp = "&";
            $url = $url.$amp.$name."=".$value;
        }
        return self::linkToAlias($url);
    }

    public static function deleteGET($url, $name, $amp = true) {
        $url = str_replace("&amp;", "&", $url);
        list($url_part, $qs_part) = array_pad(explode("?", $url), 2, "");
        parse_str($qs_part, $qs_vars);
        unset($qs_vars[$name]);
        if (count($qs_vars) != 0) {
            $url = $url_part."?".http_build_query($qs_vars);
            if ($amp) $url = str_replace("&", "&amp;", $url);
        }
        else $url = $url_part;
        return self::linkToAlias($url);
    }

    public static function addID($url, $id) {
        return $url."#".$id;
    }

    private static function linkToAlias($uri, $address = "") {
        $uri = UseSEF::replaceSEF($uri, $address);
        return $uri;

    }

}
