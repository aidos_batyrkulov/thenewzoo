<script>
    class Category {
        constructor (ID, name, parentID, level, subcats=false, checked = false) {
            this.ID = ID;
            this.name = name;
            this.parentID = parentID;
            this.level = level;
            this.subcats = subcats;
            this.checked = checked;
        }

        check() {
            let ids;
            if (localStorage.hasOwnProperty('checkedSubCategoriesIds')) {
                ids = JSON.parse(localStorage.checkedSubCategoriesIds);
                for (let i=0; i<ids.length; i++) {
                    if (this.ID===ids[i])
                        this.checked = true;
                }
            }
        }

        buildHtml(clickSubCategory) {
            let res;
            if (this.parentID>0) {
                let label = document.createElement('label');
                label.classList.add('checkCategory');
                let input = document.createElement('input');
                input.classList.add('chCategory');
                input.type = 'checkbox';
                input.addEventListener('click', ()=>{ clickSubCategory(this.ID) });
                if (this.checked)
                    input.checked = true;
                let span = document.createElement('span');
                span.textContent = this.name;
                label.append(input);
                label.append(span);
                res = label;
            } else {
                let h1 = document.createElement('h1');
                h1.textContent = this.name;
                res = h1;
            }

            return res;
        }

        static getCategories(callBack) {
            sendRequest('/main/getCategories', '', response=>{
                alert(response.responseText);
                let categories = JSON.parse(response.responseText);
                let arrCategoryObjects = [];
                for (let key in categories)  {
                    let arrSubCactegoryObjects = [];
                    for (let key2 in categories[key]['subcts']) {
                        arrSubCactegoryObjects.push(new Category(
                            key2,
                            categories[key]['subcts'][key2].name,
                            categories[key]['subcts'][key2].parent_id,
                            categories[key]['subcts'][key2].level,
                        ));
                    }
                    arrCategoryObjects.push(new Category(
                        key,
                        categories[key].name,
                        categories[key].parent_id,
                        categories[key].level,
                        arrSubCactegoryObjects
                    ));
                }
                callBack(arrCategoryObjects);
            }, 'GET');
        }
    }




    class Brand {
        constructor (ID, name, level, checked=false) {
            this.ID = ID;
            this.name = name;
            this.level = level;
            this.checked = checked;
        }

        check() {
            if (localStorage.checkedBrandsIds) {
                let ids = JSON.parse(localStorage.checkedBrandsIds!==null);
                for (let i=0; i<ids.length; i++) {
                    if (this.ID===ids[i])
                        this.checked = true;
                }
            }
        }

        buildHtml(clickBrand) {
            let res;
            let dirBrand = '<?=$dir_brand_for_frontend?>';
            let label = document.createElement('label');
            label.classList.add('checkBrand');
            let input = document.createElement('input');
            input.classList.add('chBrand');
            input.type = 'checkbox';
            input.addEventListener('click', ()=>{ clickBrand(this.ID) });
            if (this.checked)
                input.checked = true;
            let span = document.createElement('span');
            span.style.backgroundImage = `url('${dirBrand}${this.ID}.png')`;
            label.append(input);
            label.append(span);
            res = label;

            return res;
        }

        static getBrandsOnCheckedSubCategoriesOrAllBrands(callBack) {
            let args = ((localStorage.hasOwnProperty('checkedSubCategoriesIds')) ? 'checked_sub_categories='+encodeURIComponent(localStorage.checkedSubCategoriesIds) : '' );
            let api = ((localStorage.hasOwnProperty('checkedSubCategoriesIds')) ? 'getBrandsOnCheckedSubCategories' : 'getAllBrands' );
            sendRequest('/main/'+api, args, response=> {
                let brands = JSON.parse(response.responseText);
                let arrBrandObjects = [];
                if (Object.keys(brands).length) {
                    for (let key in brands) {
                        arrBrandObjects.push(new Brand(
                            key,
                            brands[key].name,
                            brands[key].level
                        ));
                    }
                }
                callBack(arrBrandObjects);
            }, 'POST');
        }
    }


    function checkCategories(arrCategoryObjects) {
        arrCategoryObjects.forEach(category=>{
            category.subcats.forEach(subCategory=>{
                subCategory.check();
            });
        });
        return arrCategoryObjects;
    }

    function checkBrands(arrBrandObjects) {
        arrBrandObjects.forEach(brand=>{
            brand.check();
        });
        return arrBrandObjects;
    }

    function displayCategories(arrCategoryObjects) {
        let leftCategory = document.createElement('div');
        leftCategory.classList.add('leftCategory');
        arrCategoryObjects.forEach(category=>{
            leftCategory.append(category.buildHtml());
            category.subcats.forEach(subCategory=>{
                leftCategory.append(subCategory.buildHtml(clickSubCategory));
                leftCategory.append(' ');
            });
        });
        MainJsController.appendToContent(leftCategory);
    }

    function displayBrands(arrBrandObjects) {
        let rightBrand = document.createElement('div');
        rightBrand.classList.add('rightBrand');
        rightBrand.append(document.createElement('h1'));
        arrBrandObjects.forEach(brand=>{
            rightBrand.append(brand.buildHtml(clickBrand));
            rightBrand.append(' ');
        });
        MainJsController.appendToContent(rightBrand);
    }


    // listeners
    function clickSubCategory(id) {

    }

    function clickBrand(id) {
        //
    }



    function startPage() {
        Category.getCategories(arrCategoryObjects=> {
            arrCategoryObjects  = checkCategories(arrCategoryObjects);
            Brand.getBrandsOnCheckedSubCategoriesOrAllBrands(arrBrandObjects=>{
                arrBrandObjects = checkBrands(arrBrandObjects);
                displayBrands(arrBrandObjects);
            });
            displayCategories(arrCategoryObjects);
        });
    }

    startPage();
</script>