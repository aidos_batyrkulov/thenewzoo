<!-- >>>>> HEADER >>>>> -->
<script>
	class LineMenuItemBuilder {
		constructor(name, link, active=0, counter=0) {
			this.name = name;
			this.link = link;
			this.active = active;
			this.counter = counter;
		}

		buildHtml() {
			let div = document.createElement('div');
			div.classList.add('headerItem', this.name);
			if (this.active) div.classList.add('active');
			div.onclick = ()=>{ location.href=this.link }
			let span = document.createElement('span');
			span.classList.add('counter');
			span.style.display = ((this.counter) ? 'inline' : 'none');
			span.textContent = this.counter;
			div.append(span);
			return div;
		}
	}

	class BlockMenuItemBuilder {
		constructor(name, link, social=0) {
			this.name = name;
			this.link = link;
			this.social = social;
		}

		buildHtml() {
			let res;
			if (this.social) {
				let a = document.createElement('a');
				a.href = this.link;
				let span = document.createElement('span');
				span.classList.add('social', this.name);
				a.append(span);
				res = a;
			} else {
				let div = document.createElement('div');
				div.classList.add(this.name);
				div.onclick = ()=>{ location.href=this.link }
				res = div;
			}
			return res;
		}
	}

	class TopBuilder {
		constructor(data) {
			this.logo = data.lineMenu.logo;
			this.lineMenuItems = data.lineMenu.items;
			this.blockMenuButton = data.lineMenu.blockMenuButton;
			this.btnCloseBlockMenuName = data.blockMenu.btnCloseName;
			this.blockMenuItemsBlockName = data.blockMenu.itemsBlockName;
			this.blockMenuItems = data.blockMenu.items;
		}

		_buildLineMenuHtml () {
			let div = document.createElement('div');
			div.classList.add('header');
			if (this.logo) {
				let divLogo = document.createElement('div');
				divLogo.classList.add('headerLogo');
				div.append(divLogo);
			}
			this.lineMenuItems.forEach(item=>{
				let itemObj = new LineMenuItemBuilder(item.name, item.link, item.active, item.counter);
				div.append(itemObj.buildHtml());
			});
			let divForBtn = document.createElement('div');
			divForBtn.classList.add('headerMenu');
			divForBtn.onclick = ()=>{  toggle(modalMenu) }
			divForBtn.style.display = ((this.blockMenuButton) ? 'block' : 'none');
			divForBtn.append(document.createElement('div'));
			div.append(divForBtn);

			return div;
		}
		_buildBlockMenuHtml () {
			let div = document.createElement('div');
			div.classList.add('modal', 'menu');
			div.id = 'modalMenu';
			div.style.display = 'none';

			let divForbtnCloseBlockMenu = document.createElement('div');
			divForbtnCloseBlockMenu.classList.add(this.btnCloseBlockMenuName);
			divForbtnCloseBlockMenu.onclick = ()=>{ toggle(modalMenu) }
			div.append(divForbtnCloseBlockMenu);

			let divForItems = document.createElement('div');
			divForItems.classList.add(this.blockMenuItemsBlockName);
			this.blockMenuItems.forEach(item=>{
				let itemObj = new BlockMenuItemBuilder(item.name, item.link, item.social);
				divForItems.append(itemObj.buildHtml());
			});
			div.append(divForItems);

			return div;
		}

		buildTopHtml() {
			let lineMenu = this._buildLineMenuHtml();
			let blockMenu = this._buildBlockMenuHtml();
			let div = document.createElement('div');
			div.classList.add('headerCover');
			let span =  document.createElement('span');
			span.append(lineMenu);
			span.append(div);
			span.append(blockMenu);
			return span;
		}
	}

	let lineMenu = JSON.parse('<?=json_encode($lineMenu,JSON_UNESCAPED_UNICODE)?>');
	let blockMenu = JSON.parse('<?=json_encode($blockMenu,JSON_UNESCAPED_UNICODE)?>');
	let topData = { lineMenu: lineMenu, blockMenu: blockMenu }
	MainJsController.data.top=topData;
	let topBuilder = new TopBuilder(topData);
	MainJsController.setTop(topBuilder.buildTopHtml());
	<?=(($alert) ? 'MainJsController.showAlert("'.$alert.'")' : '' )?>
</script>