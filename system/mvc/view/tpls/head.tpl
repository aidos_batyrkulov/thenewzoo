<!-- head_default-BEGIN -->
<head>
    <title><?=$title?></title>

    <?php foreach ($styles as $value) { ?>
        <link href="<?=$config->dir_css_for_frontend.$value?>.css" type="text/css" rel="stylesheet">
    <?php } ?>

    <?php foreach ($jses as $value) { ?>
        <script src="<?=$config->dir_js_for_frontend.$value?>.js"></script>
    <?php } ?>

    <?php foreach ($metaTags as $value) { ?>
        <?php $args=''?>
        <?php foreach ($value as $key => $val) { ?>
            <?php $args.=$key.'="'.$val.'" '?>
        <?php } ?>
        <meta <?=$args?> />
    <?php } ?>

    <?php foreach ($links as $value) { ?>
        <?php $args=''?>
        <?php foreach ($value as $key => $val) { ?>
            <?php $args.=$key.'="'.$val.'" '?>
        <?php } ?>
        <link <?=$args?> />
    <?php } ?>

    <script>
        onerror = errorHandler;
        function errorHandler(message, url, line)
        {
            let out  = "К сожалению, обнаружена ошибка.\n\n";
            out += "Ошибка: " + message + "\n";
            out += "URL: "   + url + "\n";
            out += "Строка: "  + line + "\n\n";
            out += "Щелкните на кнопке OK для продолжения работы.\n\n";
            alert(out);
            return false;
        }

        function toggle(el) {el.style.display = (el.style.display == 'none') ? '' : 'none'}

        class MainJsController {
            static data={};

            static setTop (top) {
                document.getElementById('top-span').append(top);
            }

            static appendToContent(element) {
                document.getElementById('content-span').append(element);
            }

            static showAlert(text) {
                let alertDiv;
                alertDiv = document.getElementById('modalAlert');
                if (alertDiv) {
                    alertDiv.textContent = text;
                    toggle('modalAlert');
                } else {
                    alertDiv = document.createElement('div');
                    alertDiv.classList.add('alert');
                    alertDiv.id = 'modalAlert';
                    alertDiv.onclick = () => { toggle('modalAlert') }
                    alertDiv.textContent = text;
                    MainJsController.appendToContent(alertDiv);
                }
            }
        }

        function defaultAjaxError() {alert('Проверьте подключение к интернету')}

        function createRequest() {
            let request = false;
            if (window.XMLHttpRequest) {
                request = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                try {
                    request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (CatchException) {
                    request = new ActiveXObject("Msxml2.XMLHTTP");
                }
            }
            if (!request) {
                alert("Невозможно создать XMLHttpRequest");
            }
            return request;
        }


        function sendRequest(url, args, succsess, method='POST', before=false, after=false, error=defaultAjaxError) {
            let request = createRequest();
            if (!request) {
                return;
            }
            request.onreadystatechange = function() {
                if (request.readyState == 4) {
                    if (request.status == 200) {
                        if (request.responseText==='API_SERVER_ERROR')
                            alert('responseText');
                        else
                            succsess(request);
                    } else {
                        error();
                    }
                    if (after) after();
                } else {
                    //Оповещаем пользователя о загрузке
                }
            }

            if (method.toLowerCase() == "get" && args.length > 0)
                url += "?" + args;
            request.open(method, url, true);

            if (before) before();
            if (method.toLowerCase() == "post") {
                request.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
                request.send(args);
            } else {
                request.send(null);
            }
        }
    </script>

</head>
<!-- head_default-END-->

