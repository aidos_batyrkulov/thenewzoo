<?php
return [
    'error_unknown'=> 'Неизвестная ошибка!',
    'error_empty_value'=> 'Значение не должно быть пустым',
    'error_max_len'=> 'Значение превышает максимальную длину'
];