<?php

class top extends absMyModule {

    public function __construct() {
        parent::__construct();
        $this->add('lineMenu', null, true);
        $this->add('blockMenu', null, true);
        $this->add('alert');
    }

    public function getTplFile() {
        return 'top';
    }

}
