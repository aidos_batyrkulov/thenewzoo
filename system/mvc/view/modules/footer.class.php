<?php

class footer extends absMyModule {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function getTplFile() {
		return 'footer';
	}
	
}
