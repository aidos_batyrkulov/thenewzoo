<?php

class html extends absMyModule {
	
	public function __construct() {
		parent::__construct();
		$this->add('head');
        //$this->add('hornav');
        $this->add('top');
        //$this->add('left');
        $this->add('content');
        $this->add('footer');
	}
	
	public function getTplFile() {
		return 'html';
	}
	
}
