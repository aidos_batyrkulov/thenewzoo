<?php

class main extends absMyModule {
	
	public function __construct() {
		parent::__construct();
        $this->add('dir_brand_for_frontend');
	}
	
	public function getTplFile() {
		return 'main';
	}
	
}
