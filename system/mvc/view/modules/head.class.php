<?php
class head extends absMyModule {

    public function __construct() {
        parent::__construct();
        $this->add('title');
        $this->add('styles', null, true);
        $this->add('jses', null, true);
        $this->add('metaTags', null, true);
        $this->add('links', null, true);
    }

    public function getTplFile() {
        return 'head';
    }

}