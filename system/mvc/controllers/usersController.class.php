<?php
class usersController extends absMyController {
    public function __construct(myWebSite $myWebSite) {
        parent::__construct($myWebSite);
    }

    protected function generateContent() {
        $this->lang->load('Users');

        $this->myWebSite->setTitle('Users');

        $users = new users();
        $this->myWebSite->setContent($users);
    }
}