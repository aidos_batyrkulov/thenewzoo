<?php
class mainController extends absMyController {

    public function __construct(myWebSite $myWebSite) {
        parent::__construct($myWebSite);
    }

    public static function isAccess() {
        $myAuth = myAuth::getInstance();
        return ($myAuth->getRole()=='buyer');
    }

    protected function generateContent() {
        $this->lang->load('main');
        $this->myWebSite->setTitle('Main');
        $this->myWebSite->setCurrentPageName('main');

        //$hornav = $this->getBaseHornav();
        //$this->myWebSite->setHornav($hornav);

        $main = new main();
        $main->dir_brand_for_frontend = $this->config->dir_brand_for_frontend;
        $this->myWebSite->setContent($main);
    }

    public static function getCategories() {
        $categories = categories::getAllParentCategoriesWithChildrenAndOrder();
        $arrCts = [];
        foreach ($categories as $category) {
            $arrCts[$category->id]['name'] = $category->name;
            $arrCts[$category->id]['parent_id'] = $category->parent_id;
            $arrCts[$category->id]['level'] = $category->level;
            $arrChild =[];
            foreach ($category->children as $child) {
                $arrChild[$child->id]['name'] = $child->name;
                $arrChild[$child->id]['parent_id'] = $child->parent_id;
                $arrChild[$child->id]['level'] = $child->level;
            }
            $arrCts[$category->id]['subcts'] = $arrChild;
        }
        $json = json_encode($arrCts, JSON_UNESCAPED_UNICODE);
        if ($json) return $json; else return 'API_SERVER_ERROR';
    }

    public static function getBrandsOnCheckedSubCategories() {
        $objs = self::getObjectsForApiFunctions();
        $checkedSubCatsIds =  json_decode($objs['request']->post['checked_sub_categories'], true);
        if (!isset($checkedSubCatsIds[0])) exit('{}');
        $brandSubcats = brandSubcats::getAllOnSubcatIds($checkedSubCatsIds);
        $ids =[];
        foreach ($brandSubcats as $key => $value) {$ids[]=$key;}
        $brands = brands::getBrandsOnIDsWithOrder($ids);

        if (count($brands)) {
            $brandsArr = [];
            foreach ($brands as $brand) {
                $brandsArr[$brand->id]['name'] = $brand->name;
                $brandsArr[$brand->id]['level'] = $brand->level;
            }
            $json = json_encode($brandsArr, JSON_UNESCAPED_UNICODE);
        }  else
            $json = '{}';
        return $json;
    }

    public static function getAllBrands() {
        $allBrands = brands::getAllBrandsWithOrder();
        $brandsArr = [];
        foreach ($allBrands as $brand) {
            $brandsArr[$brand->id]['name'] = $brand->name;
            $brandsArr[$brand->id]['level'] = $brand->level;
        }
        $json = json_encode($brandsArr, JSON_UNESCAPED_UNICODE);
        return $json;
    }
}