<?php
class notFound404Controller extends absMyController {
    public function __construct(myWebSite $myWebSite) {
        parent::__construct($myWebSite);
    }

    protected function generateContent() {
        $this->lang->load('404');

        $this->myWebSite->setTitle('Page not found');
        $this->myWebSite->setCurrentPageName('main');


        $main = new main();
        $main->totalUsers=5;
        $main->totalMessages=32443;
        $this->myWebSite->setContent($main);
    }
}