<?php
class promoCodes extends absMyModel {

    protected static $table = 'promo_codes';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('generated_date','validateDate', $this->getDate(), true, self::TYPE_TIMESTAMP);
        $this->add('used_date','validateDate', 0, false, self::TYPE_TIMESTAMP);
        $this->add('code', 'validateRandomCod');
        $this->add('summ', 'validateUnsignedBalance');
        $this->add('seller_id', 'validateID', 0, false);
    }


}