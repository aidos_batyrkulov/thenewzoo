<?php
class products extends absMyModel {

    protected static $table = 'products';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('name', 'validateTitle');
        $this->add('des', 'validateDes');
        $this->add('group_id', 'validateID');
        $this->add('brand_id', 'validateID');
        $this->add('photo', 'validatePdPhoto');
        $this->add('level', 'validateLevel',0,false);
    }


}