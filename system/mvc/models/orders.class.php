<?php
class orders extends absMyModel {

    protected static $table = 'orders';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('shop_id', 'validateID');
        $this->add('buyer_id', 'validateID');
        $this->add('cart_code', 'validateCartCode');
        $this->add('country', 'validateCountry','',false);
        $this->add('city', 'validateCity','',false);
        $this->add('street_home', 'validateStreetHome','',false);
        $this->add('delType', 'validateDeliveryType');
        $this->add('delPrice', 'validateUnsignedBalance');
        $this->add('summ', 'validateUnsignedBalance');
        $this->add('discount', 'validateDiscount');
        $this->add('date' ,'validateDate', $this->getDate(), true, self::TYPE_TIMESTAMP);
        $this->add('seen', 'validateActive',0,false);
        $this->add('confirmed', 'validateActive',0,false);
        $this->add('seller_paid', 'validateUnsignedBalance',0,false);
        $this->add('code_for_com', 'validateRandomCode');
        $this->add('ip', 'validateIP', $this->getIP(), true, self::TYPE_IP);
    }

    public static function getCountOfNewOrdersOnShopID ($shop_id) {
        return self::getCountOnWhere(self::$table, '`seen`='.self::$db->getSQ().' AND `shop_id`='.self::$db->getSQ(), [0,$shop_id] );
    }

}