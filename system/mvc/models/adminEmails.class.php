<?php
class adminEmails extends absMyModel {

    protected static $table = 'admin_emails';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('email', 'validateEmail');
        $this->add('type', 'validateSystemText');
    }


}