<?php
class prices extends absMyModel {

    protected static $table = 'prices';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('shop_id','validateID');
        $this->add('pd_code', 'validatePdCode');
        $this->add('price', 'validateUnsignedBalance');
        $this->add('quantity', 'validateCount', 0, false);
    }

    public static function getCountWherePriceOrQuantityIsZeroOnShopID ($shop_id) {
        return self::getCountOnWhere(self::$table, '(`price`='.self::$db->getSQ().' OR `quantity`='.self::$db->getSQ().') AND `shop_id`='.self::$db->getSQ(), [0,0,$shop_id]);
    }
    public static function areThereAtLeastOnePriceWherePriceOrQuantityIsZeroOnShopID ($shop_id) {
        return self::$db->isRowExist(self::$table, '(`price`='.self::$db->getSQ().' OR `quantity`='.self::$db->getSQ().') AND `shop_id`='.self::$db->getSQ(), [0,0,$shop_id]);
    }
}