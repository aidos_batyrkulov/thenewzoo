<?php
class shops extends passwordField {

    protected static $table = 'shops';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('seller_id', 'validateID');
        $this->add('phone', 'validatePhone');
        $this->add('name', 'validateShopName');
        $this->add('map_label', 'validateMapLabel', '', false);
        $this->add('country', 'validateCountry', '', false);
        $this->add('city', 'validateCity', '', false);
        $this->add('street_home', 'validateStreetHome', '', false);
        $this->add('type', 'validateSystemText', 'internet');
        $this->add('price_on', 'validateActive',1, false);
        $this->add('rating', 'validateRating',0, false);
        $this->add('active`','validateActive',0, false);
        $this->add('date' ,'validateDate', $this->getDate(), true, self::TYPE_TIMESTAMP);

        $this->add('ex_radius  ', 'validateExRadius',0, false);
        $this->add('ex_min_summ', 'validateUnsignedBalance',0, false);
        $this->add('ex_price   ', 'validateUnsignedBalance',0, false);
        $this->add('ex_free', 'validateUnsignedBalance',0, false);

        $this->add('td_get_order_until', 'validateTdAndTmGetOrderUntil',0, false);
        $this->add('td_min_summ', 'validateUnsignedBalance',0, false);
        $this->add('td_price', 'validateUnsignedBalance',0, false);
        $this->add('td_free', 'validateUnsignedBalance',0, false);

        $this->add('tm_get_order_until', 'validateTdAndTmGetOrderUntil',0, false);
        $this->add('tm_min_summ', 'validateUnsignedBalance',0, false);
        $this->add('tm_price', 'validateUnsignedBalance',0, false);
        $this->add('tm_free', 'validateUnsignedBalance',0, false);

        $this->add('ukr_pay', 'validateUkrPay','', false);
        $this->add('ukr_min_summ', 'validateUnsignedBalance',0, false);
        $this->add('ukr_price', 'validateUnsignedBalance',0, false);
        $this->add('ukr_free', 'validateUnsignedBalance',0, false);

        $this->add('d1_start', 'validateShopOpenTime',0, false);
        $this->add('d2_start', 'validateShopOpenTime',0, false);
        $this->add('d3_start', 'validateShopOpenTime',0, false);
        $this->add('d4_start', 'validateShopOpenTime',0, false);
        $this->add('d5_start', 'validateShopOpenTime',0, false);
        $this->add('d6_start', 'validateShopOpenTime',0, false);
        $this->add('d7_start', 'validateShopOpenTime',0, false);

        $this->add('d1_end', 'validateShopCloseTime',0, false);
        $this->add('d2_end', 'validateShopCloseTime',0, false);
        $this->add('d3_end', 'validateShopCloseTime',0, false);
        $this->add('d4_end', 'validateShopCloseTime',0, false);
        $this->add('d5_end', 'validateShopCloseTime',0, false);
        $this->add('d6_end', 'validateShopCloseTime',0, false);
        $this->add('d7_end', 'validateShopCloseTime',0, false);

    }



}