<?php
class brands extends absMyModel {

    protected static $table = 'brands';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('name', 'validateTitle');
        $this->add('level', 'validateLevel', 0,false);
    }

    public static function getBrandsOnIDsWithOrder($ids) {
        return self::getAllOnIDsWithOrder($ids, 'level', false);
    }

    public static function getAllBrandsWithOrder() {
        return self::getAllWithOrder(self::$table, __CLASS__, 'level', false);
    }


}