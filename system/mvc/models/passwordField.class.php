<?php
class passwordField extends absMyModel {

    private $newPassword = null;
    private $holdingHashTemporarily = null;
    /*
     * this var is used to hold hash of the password temporarily when
     * an object of the class is loaded and some fields except the password are changed and
     * the object is saved. This is because of the password validator must check the password,
     * NOT the hash.
     * */

    public function __construct($table) {
        parent::__construct($table);
        $this->add('password','validatePassword');
    }


    protected function preValidate() {
        if(!is_null($this->newPassword))
            parent::__set('password', $this->newPassword);
        if($this->isSaved() && $this->newPassword==null) {
            $this->holdingHashTemporarily = $this->password;
            $this->password = $this->config->example_of_correct_pass;
        }
    }

    protected function postValidate() {
        if(!is_null($this->newPassword))
            parent::__set('password', $this->hash($this->newPassword));
        if($this->isSaved() && $this->newPassword==null) {
            $this->password = $this->holdingHashTemporarily;
        }
    }

    public function __set($name, $value) {
        if ($name=='password')
            $this->newPassword = $value;
        else
            parent::__set($name, $value);
    }

    public function __get($name) {
        if ($name=='password' && (!is_null($this->newPassword)))
            return $this->newPassword;
        else
            return parent::__get($name);
    }
}