<?php
class categories extends absMyModel {

    protected static $table = 'categories';

    public function __construct() {
        parent::__construct(self::$table);
            $this->add('name', 'validateTitle');
            $this->add('parent_id', 'validateID',0, false);
            $this->add('level', 'validateLevel', 0,false);
        }

    public static function getAllParentCategoriesWithChildrenAndOrder() {
        $categories = self::getAllOnField(self::$table,  __CLASS__, 'parent_id',0, 'level', false);
        if (is_array($categories)) {
            foreach ($categories as &$category) {
                $children = self::getAllOnField(self::$table,  __CLASS__, 'parent_id',$category->id, 'level', false);
                $category->children = $children;
            }
            unset($category);
        }
        return $categories;
    }


}