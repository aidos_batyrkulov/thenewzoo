<?php
class stopIP extends absMyModel {

    protected static $table = 'stop_ip';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('ip', 'validateIP',$this->getIP(), true, self::TYPE_IP);
        $this->add('written_date','validateDate', $this->getDate(), true, self::TYPE_TIMESTAMP);
        $this->add('last_captcha_time','validateDate', 0, false, self::TYPE_TIMESTAMP);
        $this->add('last_ok_sms_time','validateDate', 0, false, self::TYPE_TIMESTAMP);
    }


}