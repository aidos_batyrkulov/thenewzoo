<?php
class sellers extends passwordField {

    protected static $table = 'sellers';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('phone','validatePhone');
        $this->add('fio','validateFio');
        $this->add('email','validateEmail');
        $this->add('balance','validateBalance',0, false);
        $this->add('active','validateActive', 1, false);
        $this->add('date','validateDate', $this->getDate(), true, self::TYPE_TIMESTAMP);
    }

    public function loadOnEmail($email) {
        return $this->loadOnField('email', $email);
    }


    public static function isEmailFree ($email) {
        return self::$db->isRowExist(self::$table, '`email`='.self::$db->getSQ(), [$email]);
    }

    public static function isPhoneFree ($phone) {
        return self::$db->isRowExist(self::$table, '`phone`='.self::$db->getSQ(), [$phone]);
    }

    public static function isFioFree ($fio) {
        return self::$db->isRowExist(self::$table, '`fio`='.self::$db->getSQ(), [$fio]);
    }
}