<?php
class brandSubcats extends absMyModel {

    protected static $table = 'brand_subcats';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('brand_id', 'validateID');
        $this->add('subcat_id', 'validateID');
    }

    public static function getAllOnSubcatIds($subcatsId) {
        return self::getAllOnIDsField($subcatsId, 'subcat_id');
    }

}