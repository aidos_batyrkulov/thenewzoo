<?php
class orderPds extends absMyModel {

    protected static $table = 'order_pds';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('order_id', 'validateID');
        $this->add('pd_code', 'validatePdCode');
        $this->add('quantity', 'validateCount');
        $this->add('price', 'validateUnsignedBalance');
    }


}