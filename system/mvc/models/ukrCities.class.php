<?php
class ukrCities extends absMyModel {

    protected static $table = 'ukr_cities';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('rus_name','validateCity');
        $this->add('additional_names','validateDes');
    }


}