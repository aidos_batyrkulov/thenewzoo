<?php
class carts extends absMyModel {

    protected static $table = 'carts';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('cart_code', 'validateCartCode');
        $this->add('pd_code', 'validatePdCode');
        $this->add('quantity','validateCount', 1);
        $this->add('date' ,'validateDate', $this->getDate(), true, self::TYPE_TIMESTAMP);
    }

    public static function getCountOfSummOfAllQuantitiesOnCartCode($cartCode) {
        $select = new select();
        $select->from(self::$table, 'quantity')
            ->where('`cart_code`='.self::$db->getSQ(),[$cartCode]);
        $arr = self::$db->selectCol($select);
        $count =0;
        if (is_array($arr)) {
            foreach ($arr as $item) {
                $count+=$item;
            }
        }
        return $count;
    }

}