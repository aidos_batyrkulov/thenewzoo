<?php
class discounts extends absMyModel {

    protected static $table = 'discounts';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('shop_id', 'validateID');
        $this->add('discount', 'validateDiscount');
        $this->add('summ', 'validateUnsignedBalance');
    }


}