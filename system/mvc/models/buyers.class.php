<?php
class buyers extends absMyModel {

    protected static $table = 'buyers';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('phone','validatePhone');
        $this->add('fio','validateFio');
        $this->add('email','validateEmail');
        $this->add('summ','validateSumm',0, false);
        $this->add('active','validateActive',1, false);
        $this->add('count_ord','validateCount',0, false);
        $this->add('date','validateDate', $this->getDate(), true, self::TYPE_TIMESTAMP);
    }
}