<?php
class authTokens extends absMyModel {

    protected static $table = 'auth_tokens';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('seller_id', 'validateID',0,false);
        $this->add('shop_id', 'validateID',0,false);
        $this->add('serial_code', 'validateRandomCode');
        $this->add('token', 'validateRandomCode');
        $this->add('ip', 'validateIP', 0, false);
        $this->add('user_agent', 'validateSystemComment');
        $this->add('del_time','validateDate', $this->getDate(time()+2592000), true, self::TYPE_TIMESTAMP);
    }


}