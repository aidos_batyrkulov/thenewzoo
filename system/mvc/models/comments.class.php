<?php
class comments extends absMyModel {

    protected static $table = 'comments';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('order_id', 'validateID');
        $this->add('name', 'validatePersonName', '', false);
        $this->add('comment', 'validateDes');
        $this->add('date','validateDate', $this->getDate(), true, self::TYPE_TIMESTAMP);
        $this->add('ball', 'validateBallForShop');
    }


}