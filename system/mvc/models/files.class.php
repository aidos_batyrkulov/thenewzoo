<?php
class files extends absMyModel {

    protected static $table = 'files';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('name', 'validateFileName');
        $this->add('date' ,'validateDate', $this->getDate(), true, self::TYPE_TIMESTAMP);
        $this->add('shop_id', 'validateID',0,false);
    }


}