<?php
class adminSettings extends absMyModel {

    protected static $table = 'admin_settings';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('data','validateSystemText');
        $this->add('type','validateSystemText');
    }

    public function loadOnType($type) {
        return $this->loadOnField('type', $type);
    }
}