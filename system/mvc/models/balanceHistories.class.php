<?php
class balanceHistories extends absMyModel {

    protected static $table = 'balance_histories';

    public function __construct() {
        parent::__construct(self::$table);
        $this->add('seller_id', 'validateID');
        $this->add('summ', 'validateUnsignedBalance');
        $this->add('balance_rest', 'validateBalance');
        $this->add('type', 'validateSystemText');
        $this->add('date','validateDate', $this->getDate(), true, self::TYPE_TIMESTAMP);
        $this->add('comment', 'validateSystemComment');
    }


}