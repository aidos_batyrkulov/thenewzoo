<?php
class groups extends absMyModel {

    protected static $table = 'groups';

    public function __construct() {
        parent::__construct(self::$table);
            $this->add('name', 'validateTitle');
            $this->add('parent_id', 'validateID',0, false);
            $this->add('level', 'validateLevel', 0,false);
            $this->add('parent_for_select', 'validateActive', 0,false);
    }


}